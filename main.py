import requests
from bs4 import BeautifulSoup
import csv

class Person:
    name = str
    description = str
    nationality = str

    def __init__(self, name, nationality, description):
        self.name = name
        self.nationality = nationality
        self.description = description

    def __repr__(self):
        return str(self.__dict__)

    def __iter__(self):
        return iter([self.name, self.nationality, self.description])

headers = {
    'authority': 'www.kith.com',
    'cache-control': 'max-age=0',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.106 Safari/537.36',
    'sec-fetch-dest': 'document',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'accept-language': 'en-US,en;q=0.9',
}

session = requests.session()

response = session.get('https://museum-alzhir.kz/ru/o-muzee/statistika', headers=headers)

soup = BeautifulSoup(response.text, 'html.parser')

f = open('museum-alzhir.csv', 'wb')

for element in soup.find_all('div', class_='leli'):

    for link in element.find_all('a'):

        url = 'https://museum-alzhir.kz'+link.get('href')

        r = session.get(url, headers=headers)

        print(url)

        s = BeautifulSoup(r.text, 'html.parser')

        mater = s.find('div', class_='mater')

        nationality = mater.find_next('h1').text.strip()

        with f:

            fnames = ['name', 'nationality', 'description']

            writer = csv.DictWriter(f, fieldnames=fnames, quotechar='|', quoting=csv.QUOTE_MINIMAL)

            writer.writeheader()

            for e in mater.find_all_next('li'):

                try:
                    name = e.find('strong').text.strip()
                except AttributeError:
                    name = ''

                description = e.text.strip()

                person = Person(name, nationality, description)

                print(person.__repr__())

                writer.writerow(list(person))